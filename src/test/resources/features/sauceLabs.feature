Feature: verificacion de precios en carrito

  @frontend
  Scenario Outline: comprar buzo y mochila con parametros
    Given que soy un "<usuario>" y "<contraseña>" standard
    When compro un buzo y una mochila
    Then reviso los precios en el carrito
  Examples:
    | usuario         | contraseña   |
    | standard_user   | secret_sauce |

