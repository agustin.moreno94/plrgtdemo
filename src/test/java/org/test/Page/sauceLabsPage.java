package org.test.Page;
import com.microsoft.playwright.Page;
import lombok.Getter;
import lombok.Setter;
import org.test.hooks.BaseTestClass;
import org.test.locators.sauceLabsLocators;

@Getter
@Setter
public class sauceLabsPage extends BaseTestClass {

    public void login (String user, String pass){
        click(sauceLabsLocators.usernameLocator);
        writeField(sauceLabsLocators.usernameLocator,user);
        click(sauceLabsLocators.passwordLocator);
        writeField(sauceLabsLocators.passwordLocator,pass);
        click(sauceLabsLocators.loginButtonLocator);
    }

    public void goToMenuAndItems(){
        check(sauceLabsLocators.inventario);
    }

    public void selectMochilaItem(){
        click(sauceLabsLocators.sauceLabsMochila);
        click(sauceLabsLocators.sauceLabsMochilaAddCart);
        click(sauceLabsLocators.backToProducts);

    }

    public void selectBuzoItem(){
        click(sauceLabsLocators.sauceLabsBuzo);
        click(sauceLabsLocators.sauceLbsBuzoAddCart);
        click(sauceLabsLocators.backToProducts);
    }

    public void goToCarrito(){
        click(sauceLabsLocators.carrito);
    }

    public String[] returnPrices(){
        String[] precios = new String[2];
        precios[0] = getText("29.99");
        precios[1] = getText("49.99");
        return precios;
    }





}
