package org.test.hooks;

import com.microsoft.playwright.*;
import com.microsoft.playwright.options.WaitUntilState;
import org.junit.jupiter.api.BeforeEach;
import org.test.config.config;


public class BaseTestClass  {

    Playwright playwright;
    BrowserType browserType;
    Browser browser;
    BrowserContext context;
    protected Page page;

    config config = new config();
    @BeforeEach
    public void browserStart() {
        String url = "https://www.saucedemo.com/";
        String browser = "chromium";
        String headless = "false";
        config.launchPlaywright(browser, headless);
        config.launchApplication(url);
    }

    //////////////////////METODOS ACCIONES//////////////////////////////
    public void click(String selector){
        config.getPage().waitForSelector(selector);
        config.getPage().locator(selector).click();
    }
    public void dblClick(String selector){
        config.getPage().waitForSelector(selector);
        config.getPage().locator(selector).dblclick();
    }
    public void writeField(String selector, String text){
        config.getPage().waitForSelector(selector);
        config.getPage().locator(selector).fill(text);
    }

    public void clickByText(String nameElement){
        config.getPage().locator(nameElement).isVisible();
        config.getPage().locator(nameElement).click();
    }

    public String getText(String getText){
        return config.getPage().getByText(getText).innerText();
    }

    public void check(String selector){
        config.getPage().waitForSelector(selector);
        //page.check(selector);
    }

    public void closePlaywright(){
        config.closePlaywright();
    }

}